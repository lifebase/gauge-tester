
#define DHT_PIN 13
#define ECHO_PIN 25
#define TRIGGER_PIN 26

// I do not see too much difference playing around with the delays
// for measurements between 10cm and 2m at least...
//
// Most of the time the accuracy is somewhere around 1% for
// short (10-40cm tested) and up to 5% for longer distances
// (160-200cm tested).

// do not go lower than 2
#define TRIGGER_DELAY_MIN 1000
#define TRIGGER_DELAY_MAX 1001
// do not go lower than 3
#define ECHO_DELAY_MIN 4
#define ECHO_DELAY_MAX 13

#define SAMPLE_SIZE 1

#define SAMPLE_DELAY 25
// note: the DHT does not deliver new results faster than every 2s
// specify the DHT read-ratio relative to the cycle delay
#define DHT_CYCLE_RATIO 10

#define CYCLE_DELAY 100

#define DHT_TYPE DHT22
#define FALLBACK_TEMPERATURE 20

#include <DHT_U.h>

DHT_Unified dht(DHT_PIN, DHT_TYPE);
// trigger the temperature measurement the first time
int dht_cycle_count = DHT_CYCLE_RATIO;
// Store at least the temp for the hc-sr04
float temperature = FALLBACK_TEMPERATURE;

void setup() {

    Serial.begin(115200);
    pinMode(TRIGGER_PIN, OUTPUT);
    pinMode(ECHO_PIN, INPUT);
    dht.begin();
}

static void read_dht() {

    sensor_t sensor;
    dht.temperature().getSensor(&sensor);
    Serial.print("Humidity/temperature sensor is ");
    Serial.print(sensor.name);
    Serial.println(".");

    sensors_event_t event;
    dht.temperature().getEvent(&event);
    if (isnan(event.temperature)) {
        Serial.println("Error reading temperature!");
    } else {
        temperature = event.temperature;
        Serial.print("Current temperature is ");
        Serial.print(temperature);
        Serial.println("°C.");
    }
    dht.humidity().getSensor(&sensor);
    dht.humidity().getEvent(&event);
    if (isnan(event.relative_humidity)) {
        Serial.println("Error reading humidity!");
    } else {
        Serial.print("Current humidity is ");
        Serial.print(event.relative_humidity);
        Serial.println("%");
    }
}

static void echo_distance(int sample_size, int trigger_delay, int echo_delay) {

    long unsigned water_distance_duration = 0;
    // speed of sound in air at a certain temperature in mm/s
    // in order to avoid floats
    // https://en.wikipedia.org/wiki/Speed_of_sound
    long unsigned sound_velocity_air = 331300 + 606 * temperature;
    long unsigned water_distance_value = 0;

    int real_sample_size = 0;
    for (int i = 0; i < sample_size; i++) {
        digitalWrite(TRIGGER_PIN, LOW);
        delayMicroseconds(trigger_delay);
        digitalWrite(TRIGGER_PIN, HIGH);
        delayMicroseconds(echo_delay);
        digitalWrite(TRIGGER_PIN, LOW);
        long unsigned duration = pulseIn(ECHO_PIN, HIGH, 30000);
        if (duration > 0) {
            real_sample_size += 1;
            water_distance_duration += duration;
        }
    }
    Serial.println("");
    if (real_sample_size > 0) {
        // Time the sound wave travelled back and forth
        // thus divided by two and this divided by the
        // number of valid measurements taken.
        Serial.print("The sound travelled for ");
        Serial.print(water_distance_duration);
        Serial.print("us; ");
        water_distance_value = water_distance_duration / 2 / real_sample_size * sound_velocity_air / 1000000;
        Serial.print("Sensor-distance to surface is ");
        Serial.print(water_distance_value);
        Serial.print("mm. (");
        Serial.print(real_sample_size);
        Serial.print(" sample(s);");
        Serial.print(" delays: ");
        Serial.print(trigger_delay);
        Serial.print("us to clear, ");
        Serial.print(echo_delay);
        Serial.print("us to call.)");
    } else {
        Serial.println("Error getting a valid distance measurement");
    }
}

void loop() {
    Serial.println("");
    Serial.println("--");
    if (dht_cycle_count < DHT_CYCLE_RATIO) {
        Serial.print("The current temperature is: ");
        Serial.print(temperature);
        Serial.println("°C");
        dht_cycle_count++;
    } else {
        dht_cycle_count = 0;
        read_dht();
    }
    for (int i = TRIGGER_DELAY_MIN; i <= TRIGGER_DELAY_MAX; i++) {
        for (int j = ECHO_DELAY_MIN; j <= ECHO_DELAY_MAX; j++) {
            echo_distance(SAMPLE_SIZE, i, j);
            delay(SAMPLE_DELAY);
        }
    }
    delay(CYCLE_DELAY);
}
